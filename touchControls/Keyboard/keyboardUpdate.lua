-- Define constants for keyboard dimensions and padding
local landscapeKeyboardX = 150
local landscapeKeyboardY = 400
local portraitKeyboardX = 1000
local portraitKeyboardY = 50
--local keyWidth = 50
--local keyHeight = 50
local keyPadding = 5

KeyboardOrientation="portrait"

touchKeyboard = {}

-- initialize touchKeyboard
touchKeyboard = {
	x = 0,
	y = 0
	}


-- Determine the orientation and set keyboard position accordingly
function updateKeyboardPosition()
    local width, height = love.graphics.getDimensions()
    if KeyboardOrientation=="portrait" then -- Landscape orientation
        touchKeyboard.x = landscapeKeyboardX
        touchKeyboard.y = landscapeKeyboardY
        touchKeyboard.rotation = 0
    elseif KeyboardOrientation=="landscape" then
        touchKeyboard.x = portraitKeyboardX
        touchKeyboard.y = portraitKeyboardY
        touchKeyboard.rotation = math.pi / 2 -- Rotate 90 degrees
    end
end


function updateKeyboard()

updateKeyboardPosition()
-- Check if the layout is keyboard or mouse
    if touchControls.layout == "keyboard" then
    
        -- Get current touches or mouse click
        local touches = love.touch.getTouches()
        if #touches == 0 and love.mouse.isDown(1) then -- Check if left mouse button is clicked
            -- Get mouse position
            local touchX, touchY = love.mouse.getPosition()
            touchKeyboard:touchpressed(touchX, touchY)
            --print("Mouse Position:", touchX, touchY)
            -- Call touchpressed and touchreleased methods of touchKeyboard or handle mouse input
        else
            -- Iterate over each touch
            for i, touchID in ipairs(touches) do
                -- Get touch position for each touch
                local touchX, touchY = love.touch.getPosition(touchID)
                --print("Touch Position:", touchX, touchY)
                -- Call touchpressed and touchreleased methods of touchKeyboard
            end
        end
        
        -- Call getSelectedKey to retrieve the selected key
       selectedKey = touchKeyboard:getSelectedKey()
        
        if selectedKey then
            print("Selected Key:", selectedKey)
        end
    end
    return selectedKey -- Return selectedKey
end
