-- touchControls.lua

require ("touchControls/Menu/Buttons")
require ("touchControls/Menu/DrawButton")

touchControls = {
  leftThumbstickRadius = 40,
  rightThumbstickRadius = 40,
  buttonRadius = 25*2,
  buttonDistance = 60,
  leftxaxis = 0,
  leftyaxis = 0,
  rightxaxis = 0,
  rightyaxis = 0,
  leftThumbstickPressed = false,
  rightThumbstickPressed = false,
  buttons = {}
}

touchControls.layout="arcade"

-- Enum for layout types
local LayoutType = {
	GAMEPAD = 1,
    ARCADE = 2,
    KEYBOARD = 3, -- Add KEYBOARD layout
}

require ("touchControls/Gamepad/createGamepadButtons")
require ("touchControls/Gamepad/createArcadeButtons")
require ("touchControls/Gamepad/removeLeftThumbstickButtons")
require ("touchControls/Gamepad/removeRightThumbstickButtons")
require ("touchControls/Gamepad/toggleLayout")
require ("touchControls/Gamepad/touchEvents")
require ("touchControls/Gamepad/mouseEvents")
require ("touchControls/Menu/checkMenuButtons")
require ("touchControls/Menu/IsHovered")

if enableRemoteControls==true then
	require ("touchControls/Network/receiveThumbstickAxes")
end
--require ("src/Network/sendAxes")


function touchControls:init()
  if os_string=="Linux" then
  touchControls.leftThumbstickRadius = 80
  touchControls.rightThumbstickRadius = 80
	-- Create left thumbstick
  --[[self.leftThumbstickX = love.graphics.getWidth() / 4
  --self.leftThumbstickY = love.graphics.getHeight() / 1
   self.leftThumbstickY = love.graphics.getHeight() / 2

  -- Create right thumbstick (only for gamepad layout)
  self.rightThumbstickX = love.graphics.getWidth() -600
  --self.rightThumbstickY = love.graphics.getHeight() / 1
  self.rightThumbstickY = love.graphics.getHeight() / 1.5
  --]]
else
	-- Create left thumbstick
  --self.leftThumbstickX = love.graphics.getWidth() / 4
  self.leftThumbstickX = 200
  self.leftThumbstickY = love.graphics.getHeight() / 1.2

  -- Create right thumbstick (only for gamepad layout)
  --self.rightThumbstickX = love.graphics.getWidth() -600
  self.rightThumbstickX = 700
  self.rightThumbstickY = love.graphics.getHeight() / 1.2
end
 -- Create buttons based on layout
  if touchControls.layout == "gamepad" then
    self:createGamepadButtons()
  elseif touchControls.layout == "arcade" then
    self:createArcadeButtons()
    self.rightThumbstickPressed = false
    --self.rightxaxis = 0
    --self.rightyaxis = 0
  elseif touchControls.layout == "keyboard" then
  end

end

require ("touchControls/Gamepad/updateThumbstickAxes")
require ("touchControls/Gamepad/checkButtonPress")
require ("touchControls/Gamepad/drawTouchControls")



-- Return touchControls table
return touchControls

