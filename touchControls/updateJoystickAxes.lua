-- Function to update joystick axes
function joystick:updateAxes(dt)
    -- Check if a joystick is connected
    if joystick.active ~= nil then
    	if not joystick.active==false then
    	
        -- Get values of left joystick axes
        self.leftxaxis = joystick.active:getGamepadAxis("leftx")
        self.leftyaxis = joystick.active:getGamepadAxis("lefty")
        
        -- Get values of right joystick axes
        self.rightxaxis = joystick.active:getGamepadAxis("rightx")
        self.rightyaxis = joystick.active:getGamepadAxis("righty")
        
        -- Get values of triggers
        self.tleft = joystick.active:getGamepadAxis('triggerleft')
        self.tright = joystick.active:getGamepadAxis('triggerright')

        -- Check if joystick is moved enough to register
        if self.leftxaxis > 0.2 or self.leftxaxis < -0.2 then 
            -- Handle left x-axis movement
        end
        
        if self.leftyaxis > 0.2 or self.leftyaxis < -0.2 then
            -- Handle left y-axis movement
        end

        -- Activate zoom if right trigger is pressed
        if self.tright > 0.2 then
            -- Handle right trigger press
        elseif self.tright < 0.2 then
            -- Handle right trigger release
        end

    end
end
end
