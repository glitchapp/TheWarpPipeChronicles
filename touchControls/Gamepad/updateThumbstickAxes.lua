
-- Update the existing updateThumbstickAxes function to update left thumbstick axes
function touchControls:updateLeftThumbstickAxes(x, y)
  local relativeX = x - self.leftThumbstickX
  local relativeY = y - self.leftThumbstickY
  local length = math.sqrt(relativeX^2 + relativeY^2)

  if length <= self.leftThumbstickRadius then
		if KeyboardOrientation == "portrait" then
			self.leftxaxis = relativeX / self.leftThumbstickRadius
			self.leftyaxis = relativeY / self.leftThumbstickRadius
    elseif KeyboardOrientation == "landscape" then
			self.leftyaxis = relativeX / self.leftThumbstickRadius
			self.leftxaxis = relativeY / self.leftThumbstickRadius
		end
  else
    local scaleFactor = self.leftThumbstickRadius / length
		if KeyboardOrientation == "portrait" then
			self.leftxaxis = relativeX * scaleFactor
			self.leftyaxis = relativeY * scaleFactor
    elseif KeyboardOrientation == "landscape" then
			self.leftyaxis = relativeX * scaleFactor
			self.leftxaxis = relativeY * scaleFactor
    end
  end
  
 menu_touchPressed(x, y)
  
end

-- Add a new function to update right thumbstick axes
function touchControls:updateRightThumbstickAxes(x, y)
  local relativeX = x - self.rightThumbstickX
  local relativeY = y - self.rightThumbstickY
  local length = math.sqrt(relativeX^2 + relativeY^2)

  if length <= self.rightThumbstickRadius then
    self.rightxaxis = relativeX / self.rightThumbstickRadius
    self.rightyaxis = relativeY / self.rightThumbstickRadius
  else
    local scaleFactor = self.rightThumbstickRadius / length
    self.rightxaxis = relativeX * scaleFactor
    self.rightyaxis = relativeY * scaleFactor
  end
end
