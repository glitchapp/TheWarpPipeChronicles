The Warp Pipe Chronicles (clone of a game written by https://github.com/Stabyourself https://stabyourself.net)

<img src="screenshot.webp" width=75% height=75%>

The original developer of this game is not involved in this project. This is a personal fork created to take the game to mobile phones, if you like it please support the original developer (link provided above).

All assets have been replaced or removed including music tracks and tiles.

Music track " It's my world" by Muciojad - https://opengameart.org/content/its-my-world

Added touch controls.

This is a work in progress, some mechanichs do not work yet.

Runs on LÖVE 11.4

MIT License