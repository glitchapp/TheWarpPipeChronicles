
-- Create buttons for arcade layout
function touchControls:createArcadeButtons()
  -- Clear existing buttons
  self.buttons = {}

  -- Create buttons around the left thumbstick
  
  local buttonPositions = {
    {x = self.rightThumbstickX , y = self.rightThumbstickY-100},
    {x = self.rightThumbstickX - 100, y = self.rightThumbstickY},
    {x = self.rightThumbstickX + 100, y = self.rightThumbstickY},
    {x = self.rightThumbstickX , y = self.rightThumbstickY+100},
  }

  for _, position in ipairs(buttonPositions) do
    table.insert(self.buttons, {x = position.x, y = position.y, pressed = false})
  end
  
  -- Remove any existing buttons associated with the right thumbstick
  self:removeRightThumbstickButtons()
end
