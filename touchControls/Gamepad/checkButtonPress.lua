function touchControls:checkButtonPress(x, y)
	if x==nil then x=0 end
	if y==nil then y=0 end
	
  for i, button in pairs(self.buttons) do
    local distanceToButton = math.sqrt((x - button.x)^2 + (y - button.y)^2)

     if distanceToButton <= self.buttonRadius then
      button.pressed = true
       
				if i==4 or i==8 then
						button4or8Pressed=true
						 
			elseif i==2 or i==6 then
						button2or6Pressed=true 
			elseif i==3 or i==7 then
						button3or7Pressed=true
			elseif i==1 or i==5 then
						button1or5Pressed=true
			else
			
			end
	
    end
  end

end
