-- touchControls.lua

touchControls = {
  leftThumbstickRadius = 40,
  rightThumbstickRadius = 40,
  buttonRadius = 25*2,
  buttonDistance = 60,
  leftxaxis = 0,
  leftyaxis = 0,
  rightxaxis = 0,
  rightyaxis = 0,
  leftThumbstickPressed = false,
  rightThumbstickPressed = false,
  buttons = {}
}

layout="arcade"

-- Enum for layout types
local LayoutType = {
  GAMEPAD = 1,
  ARCADE = 2,
}

-- Create buttons for gamepad layout
function touchControls:createGamepadButtons()
  -- Create buttons around the left thumbstick
  local buttonCount = 4
  local angleIncrement = 2 * math.pi / buttonCount

for i = 1, buttonCount do
  local angle = (i - 1) * angleIncrement
  local buttonX = self.leftThumbstickX + (self.leftThumbstickRadius + self.buttonDistance) * math.cos(angle)
  local buttonY = self.leftThumbstickY + (self.leftThumbstickRadius + self.buttonDistance) * math.sin(angle)

  table.insert(self.buttons, {x = buttonX, y = buttonY, pressed = false})
end

  -- Create buttons around the right thumbstick
  self:createRightThumbstickButtons()
end
--[[
-- Create buttons for arcade layout
function touchControls:createArcadeButtons()
  -- Clear existing buttons
  self.buttons = {}

  -- Create buttons around the left thumbstick
  self.rightThumbstickX = love.graphics.getWidth() / 1.3
  local buttonPositions = {
    
      {x = love.graphics.getWidth()/1.1 - 200, y = self.leftThumbstickY-100},
    {x = love.graphics.getWidth()/1.1 - 300, y = self.leftThumbstickY},
    {x = love.graphics.getWidth()/1.1 - 100, y = self.leftThumbstickY},
    {x = love.graphics.getWidth()/1.1 - 200, y = self.leftThumbstickY+100},
  }
 
  for _, position in ipairs(buttonPositions) do
    table.insert(self.buttons, {x = position.x, y = position.y, pressed = false})
  end
  
  -- Remove any existing buttons associated with the right thumbstick
  --self:removeRightThumbstickButtons()
end
--]]

-- Add a new function to remove buttons associated with the right thumbstick
function touchControls:removeRightThumbstickButtons()
  local buttonsToRemove = {}
  for i, button in ipairs(self.buttons) do
    local distanceToRightThumbstick = math.sqrt((button.x - self.rightThumbstickX)^2 + (button.y - self.rightThumbstickY)^2)
    if distanceToRightThumbstick <= self.rightThumbstickRadius + self.buttonRadius then
      table.insert(buttonsToRemove, i)
    end
  end

  -- Remove buttons associated with the right thumbstick
  for i = #buttonsToRemove, 1, -1 do
    table.remove(self.buttons, buttonsToRemove[i])
  end
end




-- Add a function to toggle between layouts
function touchControls:toggleLayout()
  if layout == "gamepad" then
    layout = "arcade"
  else
    layout = "gamepad"
  end

  -- Recreate buttons based on the new layout
  self.buttons = {}
  if layout == "gamepad" then
    self:createGamepadButtons()
  elseif layout == "arcade" then
    self:createArcadeButtons()
  end
end






function touchControls:init()
  if os_string=="Linux" then
  touchControls.leftThumbstickRadius = 80
  touchControls.rightThumbstickRadius = 80
	-- Create left thumbstick
  self.leftThumbstickX = love.graphics.getWidth() / 4
  --self.leftThumbstickY = love.graphics.getHeight() / 1
   self.leftThumbstickY = love.graphics.getHeight() / 1.5

  -- Create right thumbstick (only for gamepad layout)
  self.rightThumbstickX = love.graphics.getWidth() -300
  --self.rightThumbstickY = love.graphics.getHeight() / 1
  self.rightThumbstickY = love.graphics.getHeight() / 1.5
else
	-- Create left thumbstick
  self.leftThumbstickX = love.graphics.getWidth() / 4
  self.leftThumbstickY = love.graphics.getHeight() / 1.5

  -- Create right thumbstick (only for gamepad layout)
  self.rightThumbstickX = love.graphics.getWidth() * 3 / 4
  self.rightThumbstickY = love.graphics.getHeight() / 1.5
end
  

  -- Create buttons based on layout
  if layout == "gamepad" then
    self:createGamepadButtons()
  elseif layout == "arcade" then
    self:createArcadeButtons()
    
    --self.rightThumbstickX = nil
    --self.rightThumbstickY = nil
    self.rightThumbstickPressed = false
    self.rightxaxis = 0
    self.rightyaxis = 0
  end


-- Register touchpressed event for left and right thumbsticks
function love.touchpressed(id, x, y, dx, dy, pressure)
  local distanceToLeftThumbstick = math.sqrt((x - touchControls.leftThumbstickX)^2 + (y - touchControls.leftThumbstickY)^2)
  local distanceToRightThumbstick = math.sqrt((x - touchControls.rightThumbstickX)^2 + (y - touchControls.rightThumbstickY)^2)

  if distanceToLeftThumbstick <= touchControls.leftThumbstickRadius then
    touchControls.leftThumbstickPressed = true
    touchControls:updateLeftThumbstickAxes(x, y)
  elseif distanceToRightThumbstick <= touchControls.rightThumbstickRadius and not (layout=="arcade") then
    touchControls.rightThumbstickPressed = true
    touchControls:updateRightThumbstickAxes(x, y)
  else
    touchControls:checkButtonPress(x, y)
  end
end
  
-- Register touchmoved event for left and right thumbsticks
function love.touchmoved(id, x, y, dx, dy, pressure)
  if touchControls.leftThumbstickPressed then
    touchControls:updateLeftThumbstickAxes(x, y)
  elseif touchControls.rightThumbstickPressed then
    touchControls:updateRightThumbstickAxes(x, y)
  else
    -- Handle other touchmoved logic
  end
end



-- Register touchreleased event
function love.touchreleased(id, x, y)
  local distanceToLeftThumbstick = math.sqrt((x - touchControls.leftThumbstickX)^2 + (y - touchControls.leftThumbstickY)^2)
  local distanceToRightThumbstick = math.sqrt((x - touchControls.rightThumbstickX)^2 + (y - touchControls.rightThumbstickY)^2)

  if distanceToLeftThumbstick <= touchControls.leftThumbstickRadius then
    touchControls.leftThumbstickPressed = false
    touchControls.leftxaxis = 0
    touchControls.leftyaxis = 0
  elseif distanceToRightThumbstick <= touchControls.rightThumbstickRadius and not (layout=="arcade")  then
    touchControls.rightThumbstickPressed = false
    touchControls.rightxaxis = 0
    touchControls.rightyaxis = 0
  else
    -- If neither thumbstick was touched, handle it as a mouse release
    touchControls:handleMouseReleased(x, y)
  end
end


-- Register mousepressed event for left and right thumbsticks
function love.mousepressed(x, y, button, istouch, presses)
  local distanceToLeftThumbstick = math.sqrt((x - touchControls.leftThumbstickX)^2 + (y - touchControls.leftThumbstickY)^2)
  local distanceToRightThumbstick = math.sqrt((x - touchControls.rightThumbstickX)^2 + (y - touchControls.rightThumbstickY)^2)

  if distanceToLeftThumbstick <= touchControls.leftThumbstickRadius then
    touchControls.leftThumbstickPressed = true
    touchControls:updateLeftThumbstickAxes(x, y)
  elseif distanceToRightThumbstick <= touchControls.rightThumbstickRadius and not (layout=="arcade") then
    touchControls.rightThumbstickPressed = true
    touchControls:updateRightThumbstickAxes(x, y)
  else
    touchControls.leftThumbstickPressed = false
    if not (layout=="arcade") then
		touchControls.rightThumbstickPressed = false
	end
    touchControls:checkButtonPress(x, y)
  end
  
  
  -- Add a mousepressed event to handle layout switching

  if button == 1 then -- Left mouse button
    -- Check if the mouse click is within the layout switch button
    local switchButtonX = love.graphics.getWidth() - 100
    local switchButtonY = 20
    local switchButtonWidth = 80
    local switchButtonHeight = 30

    if x >= switchButtonX and x <= switchButtonX + switchButtonWidth and
       y >= switchButtonY and y <= switchButtonY + switchButtonHeight then
      touchControls:toggleLayout()
    end
  end

  
end

-- Register mousemoved event for left and right thumbsticks
function love.mousemoved(x, y, dx, dy, istouch)
  if touchControls.leftThumbstickPressed then
    touchControls:updateLeftThumbstickAxes(x, y)
  elseif touchControls.rightThumbstickPressed and not (layout=="arcade") then
    touchControls:updateRightThumbstickAxes(x, y)
  else
    -- Handle other mousemoved logic
  end
end

-- Register mousereleased event
function love.mousereleased(x, y, button, istouch, presses)
  local distanceToLeftThumbstick = math.sqrt((x - touchControls.leftThumbstickX)^2 + (y - touchControls.leftThumbstickY)^2)
  local distanceToRightThumbstick = math.sqrt((x - touchControls.rightThumbstickX)^2 + (y - touchControls.rightThumbstickY)^2)

  if distanceToLeftThumbstick <= touchControls.leftThumbstickRadius then
    touchControls.leftThumbstickPressed = false
    touchControls.leftxaxis = 0
    touchControls.leftyaxis = 0
  elseif distanceToRightThumbstick <= touchControls.rightThumbstickRadius and not (layout=="arcade") then
    touchControls.rightThumbstickPressed = false
    touchControls.rightxaxis = 0
    touchControls.rightyaxis = 0
  else
    touchControls:handleMouseReleased(x, y, button, istouch, presses)
  end
end



end


-- Update the existing createButtons function to create buttons around the left thumbstick
function touchControls:createButtons()
  local buttonCount = 4
  local angleIncrement = 2 * math.pi / buttonCount

  for i = 1, buttonCount do
    local angle = (i - 1) * angleIncrement
    local buttonX = self.leftThumbstickX + (self.leftThumbstickRadius + self.buttonDistance) * math.cos(angle)
    local buttonY = self.leftThumbstickY + (self.leftThumbstickRadius + self.buttonDistance) * math.sin(angle)

    table.insert(self.buttons, {x = buttonX, y = buttonY, pressed = false})
  end

  -- Create buttons around the right thumbstick
  self:createRightThumbstickButtons()
end

-- Add a new function to create buttons around the right thumbstick
function touchControls:createRightThumbstickButtons()
  local buttonCount = 4
  local angleIncrement = 2 * math.pi / buttonCount

  for i = 1, buttonCount do
    local angle = (i - 1) * angleIncrement
    local buttonX = self.rightThumbstickX + (self.rightThumbstickRadius + self.buttonDistance) * math.cos(angle)
    local buttonY = self.rightThumbstickY + (self.rightThumbstickRadius + self.buttonDistance) * math.sin(angle)

    table.insert(self.buttons, {x = buttonX, y = buttonY, pressed = false})
  end
end


love.mousepressed = function(x, y, button, istouch, presses)
  local distanceToThumbstick = math.sqrt((x - self.leftThumbstickX)^2 + (y - self.leftThumbstickY)^2)

  if distanceToThumbstick <= self.leftThumbstickRadius then
    print("Mouse pressed - Inside thumbstick")
    self.leftThumbstickPressed = true
	self:updateLeftThumbstickAxes(x, y)
    self:updateThumbstickAxes(x, y)
  else
    print("Mouse pressed - Outside thumbstick")
    self.leftThumbstickPressed = true
	self:updateLeftThumbstickAxes(x, y)
    self:checkButtonPress(x, y)
  end
end


function touchControls:handleTouchPressed(id, x, y)
  local distanceToThumbstick = math.sqrt((x - self.leftThumbstickX)^2 + (y - self.leftThumbstickY)^2)

  if distanceToThumbstick <= self.leftThumbstickRadius then
    self.thumbstickPressed = true
    self:updateThumbstickAxes(x, y)
  else
    self:checkButtonPress(x, y)
  end
end

function touchControls:handleMouseMoved(x, y, dx, dy, istouch)
  if self.thumbstickPressed or self.mouseJustPressed then
    print("Mouse moved - Thumbstick pressed")
    self:updateThumbstickAxes(x, y)
  else
    print("Mouse moved - Thumbstick not pressed")
  end
  self.mouseJustPressed = false
end


function touchControls:handleMouseReleased(x, y, button, istouch, presses)
  if not istouch then
    self.thumbstickPressed = false
    self.leftxaxis = 0
    self.leftyaxis = 0

    for _, button in pairs(self.buttons) do
      button.pressed = false
    end
  end
end


-- Update the existing updateThumbstickAxes function to update left thumbstick axes
function touchControls:updateLeftThumbstickAxes(x, y)
  local relativeX = x - self.leftThumbstickX
  local relativeY = y - self.leftThumbstickY
  local length = math.sqrt(relativeX^2 + relativeY^2)

  if length <= self.leftThumbstickRadius then
    self.leftxaxis = relativeX / self.leftThumbstickRadius
    self.leftyaxis = relativeY / self.leftThumbstickRadius
  else
    local scaleFactor = self.leftThumbstickRadius / length
    self.leftxaxis = relativeX * scaleFactor
    self.leftyaxis = relativeY * scaleFactor
  end
end

-- Add a new function to update right thumbstick axes
function touchControls:updateRightThumbstickAxes(x, y)
  local relativeX = x - self.rightThumbstickX
  local relativeY = y - self.rightThumbstickY
  local length = math.sqrt(relativeX^2 + relativeY^2)

  if length <= self.rightThumbstickRadius then
    self.rightxaxis = relativeX / self.rightThumbstickRadius
    self.rightyaxis = relativeY / self.rightThumbstickRadius
  else
    local scaleFactor = self.rightThumbstickRadius / length
    self.rightxaxis = relativeX * scaleFactor
    self.rightyaxis = relativeY * scaleFactor
  end
end

function touchControls:checkButtonPress(x, y, dt)
if dt==nil then dt=60 end
  for i, button in pairs(self.buttons) do
    local distanceToButton = math.sqrt((x - button.x)^2 + (y - button.y)^2)

    if distanceToButton <= self.buttonRadius then
      button.pressed = true
       
       
		if i==4 or i==8 then
			if mode == "title" then
			title.activemenu[title.menuitem].select()
			title.menuitem = 1
			sound:play(sound.effects["blip"])
			self.key_delay_timer = self.key_delay
		elseif mode == "game" then
			--player:drop()
			player:jump()
			player.canjump = true
		elseif mode == "editing" then
			
		end
	end
       
      -- You can handle the button press logic here
    end
  end
end

-- Update the existing draw function to draw the right thumbstick and its virtual thumbstick
function touchControls:draw()
  -- Draw left thumbstick
  love.graphics.circle("line", self.leftThumbstickX, self.leftThumbstickY, self.leftThumbstickRadius)

  -- Draw virtual thumbstick for left thumbstick when pressed and dragged
  if self.leftThumbstickPressed then
    love.graphics.circle("line", self.leftThumbstickX + touchControls.leftxaxis, self.leftThumbstickY + touchControls.leftyaxis, self.leftThumbstickRadius * 1.5)
  end

  -- Draw buttons for left thumbstick
  for i, button in pairs(self.buttons) do
			if i==6 then love.graphics.setColor(1,0,0,1)
		elseif i==2 then love.graphics.setColor(1,0,0,1)
		
		elseif i==3 then love.graphics.setColor(0,0,1,1)
		elseif i==7 then love.graphics.setColor(0,0,1,1)
		
		elseif i==4 then love.graphics.setColor(0,1,0,1)
		elseif i==8 then love.graphics.setColor(0,1,0,1)
		
		elseif i==1 then love.graphics.setColor(0,1,1,1)
		elseif i==5 then love.graphics.setColor(0,1,1,1)
		
		else love.graphics.setColor(1,1,1,1)
		end
    if button.pressed then
      love.graphics.circle("fill", button.x, button.y, self.buttonRadius)
    else
      love.graphics.circle("line", button.x, button.y, self.buttonRadius)
    end
  end

  if not (layout=="arcade") then 
  -- Draw right thumbstick
  love.graphics.circle("line", self.rightThumbstickX, self.rightThumbstickY, self.rightThumbstickRadius)

  -- Draw virtual thumbstick for right thumbstick when pressed and dragged
  if self.rightThumbstickPressed then
    love.graphics.circle("line", self.rightThumbstickX + touchControls.rightxaxis, self.rightThumbstickY + touchControls.rightyaxis, self.rightThumbstickRadius * 1.5)
  end
  end

end

-- Return touchControls table
return touchControls
