
-- Add a new function to remove buttons associated with the left thumbstick
function touchControls:removeLeftThumbstickButtons()
  local buttonsToRemove = {}
  for i, button in ipairs(self.buttons) do
    local distanceToLeftThumbstick = math.sqrt((button.x - self.leftThumbstickX)^2 + (button.y - self.leftThumbstickY)^2)
    if distanceToLeftThumbstick <= self.leftThumbstickRadius + self.buttonRadius then
      table.insert(buttonsToRemove, i)
    end
  end

  -- Remove buttons associated with the left thumbstick
  for i = #buttonsToRemove, 1, -1 do
    table.remove(self.buttons, buttonsToRemove[i])
  end
end
